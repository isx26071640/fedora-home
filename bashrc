# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
alias pushAll='cd ~ ; bash script.sh'
alias m04-push='bash /home/users/inf/hisx2/isx26071640/m04-xml/.script.sh'
alias m10-push='bash /home/users/inf/hisx2/isx26071640/m10-sgbd/.script.sh'
function git_push(){
	line=$(pwd)
	if [ $line = "/home/users/inf/hisx2/isx26071640/m04-xml" ]; then
		bash .script.sh > /dev/null && echo "ACTUALITZAT $(date "+%d-%m-%Y %H:%M")" \
			>> registerGit.log
	elif [ $line = "/home/users/inf/hisx2/isx26071640/m10-sgbd" ]; then
		bash .script.sh > /dev/null && echo "ACTUALITZAT $(date "+%d-%m-%Y %H:%M")" \
			>> registerGit.log
	else
		echo "Error: no estas al dir correcte"

        fi
}
